package com.hcl.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

	static Connection conn;

	public static Connection getDbConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/books_store", "root", "Nishi@123");
			return conn;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

}
