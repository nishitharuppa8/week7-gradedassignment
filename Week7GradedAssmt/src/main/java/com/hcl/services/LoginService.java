package com.hcl.services;

import com.hcl.beans.Login;

import com.hcl.dao.LoginDAO;

public class LoginService {
	LoginDAO unknownUser = new LoginDAO();

	public String storeNewAccount(Login info) {
		if (unknownUser.storeNewAccount(info) > 0) {
			return "Successfully Signup ";
		} else {
			return "Sorry! unable to create, Email already exists";
		}
	}

	public String verifyPassword(long contactNo, String password) {
		if (unknownUser.verifyPassword(contactNo, password) == true) {
			return "Login Successfull";
		} else {
			return "Login failed! Try again";
		}
	}

}
