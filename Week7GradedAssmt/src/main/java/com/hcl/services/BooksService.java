package com.hcl.services;

import java.util.List;

import com.hcl.beans.Books;
import com.hcl.dao.BooksDAO;

public class BooksService {

	BooksDAO booksDao = new BooksDAO();

	public List<Books> getAllBooks() {
		return booksDao.getAllBooks();

	}

}
