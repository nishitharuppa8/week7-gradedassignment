package com.hcl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.hcl.beans.Login;
import com.hcl.db.DBConnection;

public class LoginDAO {

	public int storeNewAccount(Login login) {
		try {
			Connection conn = DBConnection.getDbConnection();
			PreparedStatement preparedStatement = conn.prepareStatement("insert into login value(?,?,?,?)");

			preparedStatement.setLong(1, login.getContactNo());
			preparedStatement.setString(2, login.getEmail());
			preparedStatement.setString(3, login.getUserName());
			preparedStatement.setString(4, login.getPassword());

			return preparedStatement.executeUpdate();
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean verifyPassword(Long contactNo, String password) {
		try {
			Connection conn = DBConnection.getDbConnection();
			PreparedStatement preparedStatement = conn
					.prepareStatement("select password from login where contactNo = ?");

			preparedStatement.setLong(1, contactNo);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				String pass = resultSet.getString(1);
				if (pass.equals(pass)) {
					return true;
				}
			}
		} catch (Exception e) {

		}
		return false;

	}

}
