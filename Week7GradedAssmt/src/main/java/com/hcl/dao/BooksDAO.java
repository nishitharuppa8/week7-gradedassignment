package com.hcl.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.beans.Books;
import com.hcl.db.DBConnection;

public class BooksDAO {
	
	public List<Books> getAllBooks(){
		List<Books> listOfBooks = new ArrayList<Books>();
		try {
			Connection conn = DBConnection.getDbConnection();
			PreparedStatement preparedStatement= conn.prepareStatement("select * from books");
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				Books books = new Books();
				books.setId(resultSet.getInt(1));
				books.setTitle(resultSet.getString(2));
				books.setGenre(resultSet.getString(3));						
				listOfBooks.add(books);
			}
		}catch(Exception e){
		System.out.println(e);
		}
		return listOfBooks;
	}


}

