package com.hcl.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hcl.beans.Books;
import com.hcl.services.BooksService;
import com.hcl.services.LoginService;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Books book = new Books();
		BooksService booksService = new BooksService();
		List<Books> list = booksService.getAllBooks();
		HttpSession httpService = request.getSession();
		httpService.setAttribute("obj", list);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashBoard.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");

		LoginService loginservice = new LoginService();
		HttpSession httpSession = request.getSession();
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
		if (request.getParameter("contactNo") != null) {
			long contactNo = Long.parseLong(request.getParameter("contactNO"));
			String pass = request.getParameter("pass");
			String loginResult = loginservice.verifyPassword(contactNo, pass);
			requestDispatcher.include(request, response);
		}
		doGet(request, response);
	}

}
