package com.hcl.beans;
public class Login {
	 private long contactNo;
	 private String email;
	 private String userName;
	 private String password;
	 
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Login(long contactNo, String email, String userName, String password) {
		super();
		this.contactNo = contactNo;
		this.email = email;
		this.userName = userName;
		this.password = password;
	}
	
	public long getContactNo() {
		return contactNo;
	}
	public void setContactNo(long contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	  

}

