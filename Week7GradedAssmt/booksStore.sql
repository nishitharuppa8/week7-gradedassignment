create database books_store;

use books_store;

create table Login(contactNo varchar(10) primary key, email varchar(20),username varchar(20),password varchar(20));
create table Books(id int primary key ,title varchar(30),genre varchar(20));

insert into books values(1,"Harry_potter","Novel");
insert into books values(2,"life_without_limits","Narrative");
insert into books values(3,"Wings_of_fire","Autobiography");
insert into books values(4,"Mind_without_fear","Narrative");
insert into books values(5,"The_magical_book","fiction");
insert into books values(6,"The_guide","Novel");
insert into books values(7,"Swami_vivekananda","Biography");
insert into books values(8,"Learn_how_to_fly","Fiction");
insert into books values(9,"The_great_india_novel","Novel");
insert into books values(10,"Ignited_minds","Fiction");
 

 